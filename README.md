<h3><font color="black">Themer Store is no more</h3></p>
<h6><p>On the 26th, I formally announced the closure of Themer Store on the 31st August 2019. On the 30th, I made a blog post explaining why. Now, today, on the 31st, I've kept to my promise.
From today, onwards, you can use Store to install themes. Go into Store > Customisation > Themes to install them.</p>
<p>⠀</p>
<p>So long, and thanks for all the fish, Themer Store.</p></h6>
<p>⠀</p>
<p>⠀</p>
<p>⠀</p>
<p>⠀</p>

Original README.md:
# Themer-Store
This is the Official Themer Store, for finding Themer themes made by the feren OS Community

Be sure to check out the Wiki at https://github.com/feren/Themer-Store/wiki for instructions on downloading, installing, submitting, and making Themer themes. 
